# ------------------------------- GitLab ------------------------------- #
docker-gitlab-login:
	docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

docker-gitlab-push:
	docker push registry.gitlab.com/thorchain/tss/multi-party-ecdsa-docker

docker-gitlab-build:
	docker build -t registry.gitlab.com/thorchain/tss/multi-party-ecdsa-docker .
	docker tag registry.gitlab.com/thorchain/tss/multi-party-ecdsa-docker $$(git rev-parse --short HEAD)
