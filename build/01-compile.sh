#!/usr/bin/env bash
set -ef -o pipefail
BUILD_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
echo "Build dir ${BUILD_DIR}"
ROOT_DIR=${BUILD_DIR}/..
echo "Root dir ${ROOT_DIR}"
IMAGE_NAME=tss
echo "compiling ${IMAGE_NAME} executable"
rm -rf bin
mkdir -p bin
docker build --rm -t ${IMAGE_NAME} -f ../Dockerfile ../

echo "Creating a temporary docker container from ${IMAGE_NAME}"
id=$(docker create ${IMAGE_NAME})
echo "Copying binary from the container ID ${id}"
docker cp $id:/tss/target/release/tss_keygen ./bin
docker cp $id:/tss/target/release/tss_sign ./bin
echo "Removing the container ${id}"
docker rm -v $id
echo "Removing the image ${IMAGE_NAME}"
#docker image rm -f ${IMAGE_NAME}
ls -al ./bin

rm -rf data
mkdir -p data/data0
mkdir -p data/data1
mkdir -p data/data2
mkdir -p data/data3
cp -r ./bin/tss_keygen ./bin/tss_sign ./data/data0
cp -r ./bin/tss_keygen ./bin/tss_sign ./data/data1
cp -r ./bin/tss_keygen ./bin/tss_sign ./data/data2
cp -r ./bin/tss_keygen ./bin/tss_sign ./data/data3
echo "{\"parties\":\"4\",\"threshold\":\"2\",\"bootstrapnode\":\"/ip4/192.168.10.210/tcp/5433\",\"signerserver\":\"0.0.0.0:8321\",\"keygenserver\":\"0.0.0.0:8321\",\"partynum\":\"1\",\"nodeid\":\"1\"}" >./data/data0/paramsdocker
echo "{\"parties\":\"4\",\"threshold\":\"2\",\"bootstrapnode\":\"/ip4/192.168.10.210/tcp/5433\",\"signerserver\":\"0.0.0.0:8321\",\"keygenserver\":\"0.0.0.0:8321\",\"partynum\":\"2\",\"nodeid\":\"2\"}" >./data/data1/paramsdocker
echo "{\"parties\":\"4\",\"threshold\":\"2\",\"bootstrapnode\":\"/ip4/192.168.10.210/tcp/5433\",\"signerserver\":\"0.0.0.0:8321\",\"keygenserver\":\"0.0.0.0:8321\",\"partynum\":\"3\",\"nodeid\":\"3\"}" >./data/data2/paramsdocker
echo "{\"parties\":\"4\",\"threshold\":\"2\",\"bootstrapnode\":\"/ip4/192.168.10.210/tcp/5433\",\"signerserver\":\"0.0.0.0:8321\",\"keygenserver\":\"0.0.0.0:8321\",\"partynum\":\"4\",\"nodeid\":\"4\"}" >./data/data3/paramsdocker

export BUILDFOROS=linux
# use docker to build tsskeygenclient
docker build --rm -t tsskeygenclient --build-arg B_GOOS=darwin -f ../Dockerfile.keygenclient ../
keygenclientid=$(docker create tsskeygenclient)
echo "Copying binary from the container ID ${keygenclientid}"
docker cp $keygenclientid:/app/cmd/keygenclient/keygenclient ./bin
echo "Removing the container ${keygenclientid}"
docker rm -v $keygenclientid
